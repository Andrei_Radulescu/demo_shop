// Module dependencies.
var express = require("express")
    , http = require("http")
    , path = require("path")
    , routes = require("./routes");

var app = express();

//Error: 'app.router' is deprecated!
//Please see the 3.x to 4.x migration guide for details on how to update your app.
//https://github.com/strongloop/express/wiki/Migrating-from-3.x-to-4.x
app.use(app.router);


// All environments
app.set("port", 8080);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");


// App routes
app.get("/", routes.index);
app.get("/hello", routes.hello);

// Run server
http.createServer(app).listen(app.get("port"), function () {
    console.log("Express server listening on port " + app.get("port"));
});
